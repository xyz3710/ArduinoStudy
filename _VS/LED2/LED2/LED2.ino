﻿/*
 Name:		LED2.ino
 Created:	2015-11-16 오전 11:44:55
 Author:	xyz37
*/

const int DIVIDE = 100;

int _max = 0;
int _leds[] = { 8, 9, 10, 11, 12 };
int _current = 0;
int _delay = 0;

// the setup function runs once when you press reset or power the board
void setup() {
	_max = sizeof(_leds) / sizeof(int);

	for (int i = 0; i < _max; i++)
	{
		pinMode(_leds[i], OUTPUT);
	}

	_delay = _max * 100;
}

// the loop function runs over and over again until power down or reset
void loop() {
	digitalWrite(_leds[_current], LOW);

	_current++;

	if (_current >= _max)
	{
		_current = 0;
	}

	digitalWrite(_leds[_current], HIGH);

	if (_delay % 2 == 0)
	{
		_delay -= DIVIDE;
	}

	if (_delay == 0)
	{
		_delay = _max * DIVIDE;
	}

	delay(_delay);
	//for (int i = 0; i < _max; i++)
	//{
	//	digitalWrite(_leds[i], HIGH);
	//	delay(100);
	//	digitalWrite(_leds[i], LOW);
	//	delay(300);
	//}
}
