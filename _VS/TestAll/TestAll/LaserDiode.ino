void setup() {
	// put your setup code here, to run once:
	pinMode(4, OUTPUT);
}

void loop() {
	// put your main code here, to run repeatedly:
	for (int i = 0; i < 255; i += 5)
	{
		analogWrite(3, i);

		if (i < 127)
		{
			digitalWrite(4, LOW);
		}
		else
		{
			digitalWrite(4, HIGH);
		}

		delay(50);
	}
}
