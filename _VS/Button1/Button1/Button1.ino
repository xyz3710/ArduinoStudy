const int PULL_UP = 4;
const int PULL_DOWN = 5;
const int LED_PIN = 10;

// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(PULL_UP, INPUT);
	//pinMode(PULL_DOWN, INPUT);

	pinMode(LED_PIN, OUTPUT);

	Serial.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
	//int btn4 = digitalRead(PULL_UP);
	int btn5 = digitalRead(PULL_DOWN);

	/*
		if (btn4 == LOW)
		{
			Serial.println("4 - LOW  ");
			digitalWrite(LED_PIN, LOW);
		}
		else
		{
			Serial.println("4 - HIGH  ");
			digitalWrite(LED_PIN, HIGH);
		}
	*/

	if (btn5 == HIGH)
	{
		Serial.println("5 - HIGH  ");
		digitalWrite(LED_PIN, LOW);
	}
	else
	{
		Serial.println("5 - LOW  ");
		digitalWrite(LED_PIN, HIGH);
	}
}
