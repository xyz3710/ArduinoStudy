// �½������� - DHT11 / DHT22  - Temp & Humidity Sensors

#include "TempHumidity.h"
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int  nPin = A0;
int  tData[5] = { 0 };
bool bRes = false;

void setup()
{
	pinMode(nPin, OUTPUT);
	digitalWrite(nPin, HIGH);
}

void loop()
{
	bRes = ReadTempHumidity(nPin,tData);

	u8g.firstPage();
	do
	{
		draw();

	} while (u8g.nextPage());

	delay(500);
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	if (bRes == true)
	{
		char sTemp1[100] = "";
		char sTemp2[100] = "";
		sprintf(sTemp1, "%d.%d", tData[0], tData[1]);
		sprintf(sTemp2, "%d.%d", tData[2], tData[3]);

		u8g.drawStr(0, 10, "Humdity");
		u8g.drawStr(0, 22, sTemp1);
		u8g.drawStr(0, 40, "Temperature");
		u8g.drawStr(0, 52, sTemp2);
	}
  else
  {
    u8g.drawStr(0, 10, "Error");
  }
}

