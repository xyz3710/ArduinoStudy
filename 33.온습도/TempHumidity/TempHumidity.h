
#include <Arduino.h>

byte ReadTempHumidityData(int nReadPin)
{
  byte tRes = 0;

  for (byte i = 0; i< 8; i++)
  {
    while (digitalRead(nReadPin) == LOW);

    delayMicroseconds(30);

    if (digitalRead(nReadPin) == HIGH)
    {
      tRes |= (1 << (7 - i));
    }

    while (digitalRead(nReadPin) == HIGH);
  }

  return tRes;
}

bool ReadTempHumidity(int nReadPin, int * tReadData)
{
	byte tRead = 0;

	digitalWrite(nReadPin, LOW);
	delay(20);
	digitalWrite(nReadPin, HIGH);
	delayMicroseconds(40);

	pinMode(nReadPin, INPUT);
	tRead = digitalRead(nReadPin);
	if (tRead) return false;

	delayMicroseconds(80);
	tRead = digitalRead(nReadPin);
	if (!tRead) return false;

	delayMicroseconds(80);

	for (byte i = 0; i < 5; i++)
	{
		tReadData[i] = ReadTempHumidityData(nReadPin);
	}

	pinMode(nReadPin, OUTPUT);
	digitalWrite(nReadPin, HIGH);

	int tChkSum = tReadData[0] + tReadData[1] + tReadData[2] + tReadData[3];
	if (tReadData[4] != tChkSum) return false;

	return true;
}

