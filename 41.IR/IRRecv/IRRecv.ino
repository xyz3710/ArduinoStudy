
#include "U8glib.h"
#include <IRremote.h>


int nPin = 9;
unsigned long ulData = 0;

IRrecv ir(nPin);
U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

void setup()
{
	ir.enableIRIn();
}

void loop()
{
	decode_results res;
	if (ir.decode(&res))
	{
		if (res.value != -1) ulData = res.value;
		ir.resume();
	}

	u8g.firstPage();
	do
	{
		draw();

	} while (u8g.nextPage());
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	char sTemp1[100] = "";
	char sTemp2[100] = "";
	sprintf(sTemp1, "0x%lX", ulData);
	sprintf(sTemp2, "%lu", ulData);
	u8g.drawStr(0, 10, sTemp1);
	u8g.drawStr(0, 26, sTemp2); 
}


