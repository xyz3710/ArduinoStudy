
#include <CustomStepper.h>

CustomStepper stepper(8, 9, 10, 11);

void setup()
{
	stepper.setRPM(12);
	stepper.setSPR(4075.7728395);
}

bool bFlog = false;

void loop()
{
	if (stepper.isDone() == true)
	{
		if (bFlog == true)
		{
			stepper.setDirection(CW);	// 시계방향회전 
			stepper.rotateDegrees(90);	// 회전각도
		}
		else
		{
			stepper.setDirection(CCW);	// 시계반대방향회전
			stepper.rotateDegrees(90);	// 회전각도 
		}

		bFlog = !bFlog;
	}

	stepper.run();
}

