
void DUBeep()
{
	digitalWrite(3, HIGH);
	delay(1);
	digitalWrite(3, LOW);
	delay(1);
}

void setup()
{
	pinMode(3, OUTPUT);
	pinMode(7, INPUT);

	pinMode(12, OUTPUT);
}

void loop()
{
	int nButton = digitalRead(7);

	if (nButton != LOW)
	{
		DUBeep();
		digitalWrite(12, HIGH);
	}
	else
	{
		digitalWrite(12, LOW);
	}
}
