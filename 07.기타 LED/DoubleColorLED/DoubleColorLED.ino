void setup()
{
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
}

void loop()
{ 
  // ALL ON
  digitalWrite(6, HIGH);
  digitalWrite(7, HIGH);
  delay(1000);
  // LEFT ON
  digitalWrite(6, LOW);
  digitalWrite(7, HIGH);
  delay(1000);
  // RIGHT ON
  digitalWrite(6, HIGH);
  digitalWrite(7, LOW);
  delay(1000);
  // ALL OFF
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  delay(1000);
}


