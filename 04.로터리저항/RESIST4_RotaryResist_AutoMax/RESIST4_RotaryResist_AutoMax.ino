int _max = 0;

void setup() {
	Serial.begin(9600);
}

void loop() {
	int readValue = analogRead(A0);

  // 자동으로 최대값을 저장/설정할 수 있는 코드
  if (_max > readValue)
  {
    _max = readValue;
  }

  double val = 0;

  val = (double)readValue;
  val /= (double)_max;
  val *= (double)255;

  Serial.print(" Max: "); Serial.print(_max);
  Serial.print(" val: "); Serial.print(val);
  Serial.print("   ");    Serial.println(readValue);
  
  analogWrite(3, (int)val);
}
