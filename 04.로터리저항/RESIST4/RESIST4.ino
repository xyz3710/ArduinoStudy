
void setup()
{
	Serial.begin(9600);
}

void loop()
{
	int nRead = analogRead(A0);
	Serial.println(nRead);
}

/*
읽어오기
void setup()
{
	Serial.begin(9600);
}

void loop()
{
	int nRead = analogRead(A0);
	Serial.println(nRead);
}


*/



/*
LED색상 같이 바꾸기 
void setup()
{
	Serial.begin(9600);
}

void loop()
{
	int nRead = analogRead(A0);
	int nValue = nRead / 3;

	Serial.println(nRead);
	analogWrite(3, nValue);
}

*/

/*
로터리 값 반대로 처리하기 
void setup()
{
	Serial.begin(9600);
}

void loop()
{
	int nRead = analogRead(A0);

	nRead = 700 - nRead;
	nRead = nRead <   0 ?   0 : nRead;
	nRead = nRead > 700 ? 700 : nRead;

	int nValue = nRead / 3;

	Serial.println(nRead);
	analogWrite(3, nValue);
}

*/


/*
void setup()
{
	Serial.begin(9600);
}

int nMax = 0;

void loop()
{
	int nRead = analogRead(A0);

	if (nRead > nMax) nMax = nRead;

	double dbVal = 0;
	dbVal = (double)nRead;
	dbVal /= (double)nMax;
	dbVal *= (double)255;

	Serial.print(" nMax = "); Serial.print(nMax);
	Serial.print(" nVal = "); Serial.print(dbVal);
	Serial.print("  ");		  Serial.println(nRead);

	analogWrite(3, (int)dbVal);
}
*/