
void setup()
{
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	pinMode(8, OUTPUT);
	Serial.begin(9600);
}

void loop()
{
	int nRead = analogRead(A0);
	Serial.println(nRead);

	if (nRead < 85)
	{
		digitalWrite(6, HIGH);
		digitalWrite(7, LOW );
		digitalWrite(8, LOW );
	}
	else if (nRead < 170)
	{
		digitalWrite(6, LOW );
		digitalWrite(7, HIGH);
		digitalWrite(8, LOW );
	}
	else
	{
		digitalWrite(6, LOW );
		digitalWrite(7, LOW );
		digitalWrite(8, HIGH);
	}
}

/*
읽어오기
void setup()
{
Serial.begin(9600);
}

void loop()
{
int nRead = analogRead(A0);
Serial.println(nRead);
}


*/



/*
LED색상 같이 바꾸기
void setup()
{
Serial.begin(9600);
}

void loop()
{
int nRead = analogRead(A0);
int nValue = nRead / 3;

Serial.println(nRead);
analogWrite(3, nValue);
}

*/

/*
void setup()
{
Serial.begin(9600);
}

int nMax = 0;

void loop()
{
int nRead = analogRead(A0);

if (nRead > nMax) nMax = nRead;

double dbVal = 0;
dbVal = (double)nRead;
dbVal /= (double)nMax;
dbVal *= (double)255;

Serial.print(" nMax = ");
Serial.print(nMax);

Serial.print(" nVal = ");
Serial.print(dbVal);

Serial.print("  ");
Serial.println(nRead);
analogWrite(3, (int)dbVal);
}

*/