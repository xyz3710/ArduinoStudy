
int nMax = 0;
int nLeds[]  = { 10, 11, 12 };
int nCurrent = 0;

void setup()
{
	nMax = sizeof(nLeds) / sizeof(int);
	
	for (int i = 0; i < nMax ; i++)
	{
		pinMode(nLeds[i], OUTPUT);
	}
}

void loop()
{
	digitalWrite(nLeds[nCurrent], LOW);

	nCurrent++;
	if (nCurrent >= nMax) nCurrent = 0;
	
	digitalWrite(nLeds[nCurrent], HIGH);
	delay(500);
}
