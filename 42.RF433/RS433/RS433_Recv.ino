const int PIN = A0;
const unsigned int uHIGH = 70;
const unsigned int uLOW = 50;

void setup()
{
	Serial.begin(9600);
}

void loop()
{
	int read = analogRead(PIN);

	if (read > uHIGH)
	{
		Serial.println(read);
	}

	if (read < uLOW)
	{
		Serial.println(read);
	}

	delay(10);
}
