
int nPin =  4;  //RF Transmitter pin = digital pin 4

void setup()
{
  pinMode(nPin, OUTPUT);     
}

void loop()
{  
  digitalWrite(nPin, HIGH);     // Transmit a HIGH signal
  delay(1000);  
  digitalWrite(nPin, LOW );     // Transmit a LOW  signal
  delay(1000);  
}

