#include <RCSwitch.h>

RCSwitch rcs = RCSwitch();

void setup() 
{
  rcs.enableTransmit(10);  // Using Pin #10
}

int nCount = 0;

void loop() 
{
  SendRCS(nCount++);
  
  delay(100);  
}

void SendRCS(int nValue)
{  
  char cTemp[1024]="";
  String strTemp = String(nValue,BIN);
  strTemp.toCharArray(cTemp,strTemp.length());
  rcs.send(cTemp);  
  
  // rcs.send("10000100011000000000010100");
}

