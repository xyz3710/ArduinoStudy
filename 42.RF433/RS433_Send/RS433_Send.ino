const int PIN = 4;		// RF Transmitter

void setup()
{
	Serial.begin(9600);
}

void loop()
{
	digitalWrite(PIN, HIGH);
	delay(500);
	digitalWrite(PIN, LOW);
	delay(500);
}
