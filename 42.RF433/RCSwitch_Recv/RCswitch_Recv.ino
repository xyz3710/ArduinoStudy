#include <RCSwitch.h>

RCSwitch rcs = RCSwitch();

void setup() 
{
  Serial.begin(9600);
  rcs.enableReceive(0);  // Receiver on inerrupt 0 => that is pin #2
}

void loop() 
{
  if (rcs.available()) 
  {  
    int value = rcs.getReceivedValue();
    
    if (value == 0) 
    {
      Serial.print("Unknown encoding");
    } 
    else 
    {
      Serial.print("Received ");
      Serial.print( rcs.getReceivedValue() );
      Serial.print(" / ");
      Serial.print( rcs.getReceivedBitlength() );
      Serial.print("bit ");
      Serial.print("Protocol: ");
      Serial.println( rcs.getReceivedProtocol() );
    }
    
    rcs.resetAvailable();
  }
}
