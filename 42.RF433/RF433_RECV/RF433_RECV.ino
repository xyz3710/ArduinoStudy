
int nPin = A0;                  // RF Receiver pin = Analog pin 2
const unsigned int uHigh = 70;  // upper threshold value
const unsigned int uLow  = 50;  // lower threshold value

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  int nRead = analogRead(nPin);    
  
  if(nRead > uHigh)
  {
    Serial.println(nRead);
  }
  
  if(nRead < uLow)
  {
    Serial.println(nRead);
  }
  
  // delay(100);
}
