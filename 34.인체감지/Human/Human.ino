
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int nPin  = 9;
int nData = 0;

void setup()
{
  pinMode(nPin, INPUT);
}

void loop()
{
  nData = digitalRead(nPin);

  u8g.firstPage();
  do
  {
    draw();

  } while (u8g.nextPage());
}

void draw(void)
{
  u8g.setFont(u8g_font_unifont);

  char sTemp[100] = "";
  sprintf(sTemp, "%d", nData);

  u8g.drawStr(0, 10, sTemp);
}

