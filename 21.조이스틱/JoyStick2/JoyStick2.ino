#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int nPinX = A2; // VRx
int nPinY = A1; // VRy
int nPinZ = A0; // SW    & +5V �������� 
int nX = 0;
int nY = 0;
int nZ = 0;

int nJoyX = 0;
int nJoyY = 0;
int nJoyZ = 0;

void DrawString(int x, int y, const char * ctexts)
{
	u8g.drawStr(x * 6, y * 12 + 10, ctexts);
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	String strX = String(nX);
	String strY = String(nY);
	String strZ = String(nZ);
	String strJoyX = String(nJoyX);
	String strJoyY = String(nJoyY);
	String strJoyZ = String(nJoyZ);

	DrawString(0, 0, "X: ");
	DrawString(0, 1, "Y: ");
	DrawString(0, 2, "Z: ");

	DrawString(3, 0, strJoyX.c_str());
	DrawString(3, 1, strJoyY.c_str());
	DrawString(3, 2, strJoyZ.c_str());
	DrawString(8, 0, strX.c_str());
	DrawString(8, 1, strY.c_str());
	DrawString(8, 2, strZ.c_str());
}

void setup()
{

}

void loop()
{
	nX = analogRead(nPinX);
	nY = analogRead(nPinY);
	nZ = analogRead(nPinZ);

	nJoyX = (nX / 50) - 10;
	nJoyY = (nY / 50) - 10;
	nJoyZ =  nZ <= 1 ? 1 : 0;

	u8g.firstPage();

	do
	{
		draw();

	} while (u8g.nextPage());

	delay(100);
}


