
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int nPinX = A2;
int nPinY = A1;
int nPinZ = A0;

int nX = 0;
int nY = 0;
int nZ = 0;

int nJoyX = 0;
int nJoyY = 0;
int nJoyZ = 0;

void DrawString(int x, int y, const char * ctexts)
{
	u8g.drawStr(x * 6, y * 12 + 10, ctexts);
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	char sX[100] = "";
	char sY[100] = "";
	char sZ[100] = "";
	sprintf(sX, "X: %3d   %4d", nJoyX, nX);
	sprintf(sY, "Y: %3d   %4d", nJoyY, nY);
	sprintf(sZ, "Z: %3d   %4d", nJoyZ, nZ);

	DrawString(0, 0, sX);
	DrawString(0, 1, sY);
	DrawString(0, 2, sZ);

	double dbX = 0;
	double dbY = 0;

	dbX = ((double)nX) * 128 / 1024;
	dbY = ((double)nY) *  64 / 1024;

	dbX -= 3;
	dbY -= 3;

	if (nJoyZ == 0)
	{
		u8g.drawBox(dbX, dbY, 7, 7);
	}
	else
	{
		u8g.drawFrame(dbX, dbY, 7, 7);
	}
}

void setup()
{

}

void loop()
{

	nX = analogRead(nPinX);
	nY = analogRead(nPinY);
	nZ = analogRead(nPinZ);

	/*
	nJoyX = (nX / 40)-10;
	nJoyY = (nY / 40)-10;
	nJoyZ = nZ <= 10 ? 0 : 1;
	*/

	nJoyX = (nX / 50) - 10;
	nJoyY = (nY / 50) - 10;
	nJoyZ = nZ <= 1 ? 0 : 1;

	u8g.firstPage();

	do
	{
		draw();

	} while (u8g.nextPage());

	delay(100);
}


