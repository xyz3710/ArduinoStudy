#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

const int PIN_X = A2;		// VRx
const int PIN_Y = A1;		// VRy
const int PIN_Z = A0;		// VRz
int _x = 0;
int _y = 0;
int _z = 0;

void setup()
{
	pinMode(PIN_X, INPUT);    // analog는 궂이 pinMode를 선택하지 않아도 된다.
	pinMode(PIN_Y, INPUT);    // 하지만 digital은 pinMode를 선택하지 않으면 최대 전류를 전달하지 않아서 오작동이 발생할 수 있다. 
	pinMode(PIN_Z, INPUT);
	//Serial.begin(9600);
}

void loop()
{
	_x = analogRead(PIN_X);
	_y = analogRead(PIN_Y);
	_z = analogRead(PIN_Z);

	u8g.firstPage();

	do
	{
		draw();
	} while (u8g.nextPage());

 delay(100);
}

void drawString(int x, int y, const char* cText)
{
	u8g.drawStr(x * 6, y * 12 + 10, cText);
}

void draw()
{
	u8g.setFont(u8g_font_unifont);

	String x = String(_x);
	String y = String(_y);
	String z = String(_z);

	drawString(0, 0, "X: ");
	drawString(0, 1, "Y: ");
	drawString(0, 2, "Z: ");
	drawString(3, 0, x.c_str());
	drawString(3, 1, y.c_str());
	drawString(3, 2, z.c_str());

}
