
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int nPin  = A0;
int nData =  0;

void setup()
{

}

extern void draw(void);

void loop()
{
	nData = analogRead(nPin);

	u8g.firstPage();
	do
	{
		draw();

	} while (u8g.nextPage());

	delay(200);
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	char sTemp[100] = "";
	sprintf(sTemp, "%d", nData);

	u8g.drawStr(0, 10, sTemp);
}


