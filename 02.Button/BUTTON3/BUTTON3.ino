void setup()
{
	pinMode(10, OUTPUT);
	pinMode(4, INPUT); 
}

void loop()
{
	int nBtn4 = digitalRead(4);

	if (nBtn4 == LOW)
	{
		digitalWrite(10, LOW);
	}
	else
	{
		digitalWrite(10, HIGH);
	}
}
