void setup()
{
	pinMode( 4, INPUT ); // Pull-up
	pinMode( 5, INPUT ); // Pull-down

	Serial.begin(9600);
}

void loop()
{
	int nBtn4 = digitalRead(4);
	int nBtn5 = digitalRead(5);

	if (nBtn4 == LOW)
	{
		Serial.print("4 - LOW  ");
	}
	else 
	{
		Serial.print("4 - HIGH ");
	}

	if (nBtn5 == HIGH)
	{
		Serial.println("5 - HIGH");
	}
	else
	{
		Serial.println("5 - LOW");
	}
}
