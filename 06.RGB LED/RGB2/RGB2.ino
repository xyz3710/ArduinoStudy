

void setup()
{

}

int nG = 3;
int nR = 5;
int nB = 6;

void loop()
{
	for (int g = 0; g < 255; g += 10)
	{
		for (int r = 0; r < 255; r += 10)
		{
			for (int b = 0; b < 255; b += 10)
			{
				analogWrite(nG, g);
				analogWrite(nR, r);
				analogWrite(nB, b);
				delay(100);
			}
		}
	}

	analogWrite(nG, 255);
	analogWrite(nR, 255);
	analogWrite(nB, 255);
	delay(500);

	analogWrite(nG, 0);
	analogWrite(nR, 0);
	analogWrite(nB, 0);
	delay(500);

	analogWrite(nG, 255);
	analogWrite(nR, 255);
	analogWrite(nB, 255);
	delay(500);

	analogWrite(nG, 0);
	analogWrite(nR, 0);
	analogWrite(nB, 0);
	delay(500);
}
