void setup()
{

}

const int GREEN = 3;
const int RED = 5;
const int BLUE = 6;
const int MAX = 255;

void loop()
{
	for (int g = 0; g < MAX; g++)
	{
		for (int r = 0; r < MAX; r++)
		{
			for (int b = 0; b < MAX; b++)
			{
				analogWrite(GREEN, g);
				analogWrite(RED, r);
				analogWrite(BLUE, b);
				delay(100);
			}
		}
	}
}