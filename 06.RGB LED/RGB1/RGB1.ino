
int nMax = 0;
int nLeds[]  = { 6, 7, 8 };
int nCurrent = 0;

void setup()
{
	/*
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	pinMode(8, OUTPUT);
	*/

	nMax = sizeof(nLeds) / sizeof(int);
	
	for (int i = 0; i < nMax ; i++)
	{
		pinMode(nLeds[i], OUTPUT);
	}

}

void loop()
{
	/*
	digitalWrite(6, HIGH);
	digitalWrite(7, HIGH);
	digitalWrite(8, HIGH);
	delay(500);
	digitalWrite(6, LOW);
	digitalWrite(7, LOW);
	digitalWrite(8, LOW);
	delay(500);
	*/

	/*
	digitalWrite(6, HIGH);
	digitalWrite(7, LOW);
	digitalWrite(8, LOW);
	delay(500);
	digitalWrite(6, LOW);
	digitalWrite(7, HIGH);
	digitalWrite(8, LOW);
	delay(500);
	digitalWrite(6, LOW);
	digitalWrite(7, LOW);
	digitalWrite(8, HIGH);
	delay(500);
	*/

	digitalWrite(nLeds[nCurrent], LOW);

	nCurrent++;
	if (nCurrent >= nMax) nCurrent = 0;
	
	digitalWrite(nLeds[nCurrent], HIGH);
	delay(500);
}
