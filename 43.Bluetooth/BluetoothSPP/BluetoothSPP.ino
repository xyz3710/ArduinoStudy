
#include <SoftwareSerial.h>

SoftwareSerial bt(10, 11); // (RX=11, TX=10) 

char val;

void setup() {
  bt.begin(9600);
  Serial.begin(9600);
}

void loop() {
  /*
  if (Serial.available() > 0)
  {
  bt.println("Serial Available");
  }
  */
  if (bt.available() > 0)
  {
    val = bt.read();
    Serial.println(val);
  }

  if (Serial.available() > 0)
  {
    val = Serial.read();    // 1. Serial에서 읽어서

    if (val != 0)
    {
      //Serial.println(val);
      bt.println(val);      // 2. Bluetooth로 전송 한다.
    }
  }

  //bt.write(val);    // 변환 후 처리
  delay(10);
  val = 0;
}
