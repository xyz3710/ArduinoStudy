void setup() {
  Serial.begin(9600);
}

int _count = 0;

void loop() {
  Serial.println(_count++);

  if (_count == 512)
  {
    _count = 0;
  }
  
  delay(10);
}
