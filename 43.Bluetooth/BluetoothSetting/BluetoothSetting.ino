
#include <SoftwareSerial.h>

SoftwareSerial bt(10, 11); // (RX=11, TX=10) 

void setup()
{
	pinMode(9, OUTPUT);  // this pin will pull the XM-15B CE pin HIGH to enable the module
	digitalWrite(9, HIGH);
	Serial.begin(9600);
	Serial.println("AT Command:");
	bt.begin(9600);  // XM-15B default speed
}

void loop()
{
	// read from XM-15B ==> Serial Monitor
	if (bt.available())
 {
  Serial.println("Available");
  Serial.write(bt.read());
 }
 else
 {
  Serial.println("Not Available");
 }
 
	// read from Serial Monitor ==> XM-15B
	if (Serial.available())
	bt.write(Serial.read());

  delay(500);
}

/*
 
AT
AT+VERSION? 
AT+NAME? 
AT+NAME=XB-15B-001

 */
