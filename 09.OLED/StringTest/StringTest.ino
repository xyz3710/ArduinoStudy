#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

bool toggle = false;

void setup()
{
}

void loop()
{
  u8g.firstPage();

  do
  {
    draw();
  } while (u8g.nextPage());

  toggle != toggle;
  delay(500);
}

void draw()
{
  const int HEIGHT = 12;
  int row = 10;

  if (toggle == true)
  {
    u8g.setFont(u8g_font_unifont);
  }
  else
  {
    u8g.setFont(u8g_font_6x12);
  }

  u8g.drawStr(0, row, "qwerqwertqwertqwert");
  u8g.drawStr(0, row += HEIGHT, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  u8g.drawStr(0, row += HEIGHT, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  u8g.drawStr(0, row += HEIGHT, "Abcdefghijklmnopqrstuvwxyz");
  u8g.drawStr(0, row += HEIGHT, "12345678901234567890");
}
