#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4); 

void setup()
{
}

void loop()
{
	u8g.firstPage();
	do
	{
		draw();
	} 
	while (u8g.nextPage());
}

void draw(void)
{		
	u8g.setFont(u8g_font_unifont);
	u8g.drawStr(0, 10, "qwertyqwertyqwertyqwerty");
	u8g.drawStr(0, 22, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 34, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 46, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 58, "abcdefghijklmnopqrstuvwxyz");
	u8g.drawStr(0, 70, "12345678901234567890");
}



/*
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

void setup()
{

}

bool bFlag = false;

void loop()
{
	u8g.firstPage();
	do
	{
		draw();
	} 
	while (u8g.nextPage());

	bFlag = !bFlag;
	delay(500);
}

void draw(void)
{
	if (bFlag == true)
	{
		u8g.setFont(u8g_font_unifont);
	}
	else
	{
		u8g.setFont(u8g_font_6x12);		
	//	u8g.setFont(u8g_font_courB12); 
	}

	u8g.drawStr(0, 10, "qwertyqwertyqwertyqwerty");
	u8g.drawStr(0, 22, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 34, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 46, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	u8g.drawStr(0, 58, "abcdefghijklmnopqrstuvwxyz");
	u8g.drawStr(0, 70, "12345678901234567890");
}

*/

