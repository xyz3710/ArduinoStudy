#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

const int PIN = A0;

int _data = 0;

void setup()
{
	pinMode(PIN, INPUT);
	Serial.begin(9600);
}

void loop()
{
	_data = analogRead(PIN);

	Serial.println(_data);
	u8g.firstPage();

	do
	{
		draw();
	} while (u8g.nextPage());
}

void draw()
{
	u8g.setFont(u8g_font_unifont);

	char temp[100] = "";

	sprintf(temp, "%d", _data);

	u8g.drawStr(0, 10, temp);
}
