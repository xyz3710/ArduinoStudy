#include "MQ135.h"

MQ135 gas(A0);

void setup() {
	
	Serial.begin(9600);
}

void loop() {
  

	Serial.print(gas.getPPM()); // Get the ppm of CO2 sensed (assuming only CO2 in the air)
	Serial.print(" / ");
	Serial.print(gas.getResistance()); // Get the resistance of the sensor, ie. the measurement value
	Serial.print(" / ");
	Serial.println(gas.getRZero()); // Get the resistance RZero of the sensor for calibration purposes
	
	delay(100);
}
