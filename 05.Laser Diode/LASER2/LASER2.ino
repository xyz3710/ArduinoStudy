
void setup()
{
	pinMode(4, OUTPUT);
}

void loop()
{
	for (int i = 0; i < 255;i+=5)
	{
		analogWrite(3, i);

		if (i < 127) digitalWrite(4, LOW );
		else         digitalWrite(4, HIGH);

		delay(50);
	}	
}
