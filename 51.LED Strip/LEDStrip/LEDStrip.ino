#include <Adafruit_NeoPixel.h>

#define  PIN    A0   // PIN
#define  NUM     6   // RGB LED 
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM, PIN, NEO_GRB + NEO_KHZ800);

void setup() 
{
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

/*
void loop() 
{
  strip.setPixelColor(0,strip.Color(255,  0,  0));  
  strip.show();
  delay(500);
  
  strip.setPixelColor(0,strip.Color(0,0,0));   
  strip.show();
  delay(500);
}
*/

void loop() 
{
  for(uint16_t i=0; i<strip.numPixels(); i++) 
  {
    int nR = random(0, 255);
    int nG = random(0, 255);
    int nB = random(0, 255);
    strip.setPixelColor(i, strip.Color(nR,nG,nB));
  }
  strip.show();
  delay(50);
}


/*
void loop() 
{
  SetColor(0,strip.Color(255,  0,  0),500);
  SetColor(1,strip.Color(255,255,  0),500);
  SetColor(2,strip.Color(255,  0,255),500);
  SetColor(3,strip.Color(255,255,  0),500);
  SetColor(4,strip.Color(255,  0,255),500);
  SetColor(5,strip.Color(  0,255,255),500);
  
  Clear();
  delay(500);
}

void SetColor(int i, uint32_t c, uint8_t wait)
{
  strip.setPixelColor(i, c);  
  strip.show();
  delay(wait);
}

void Clear()
{
  for(uint16_t i=0; i<strip.numPixels(); i++) 
  {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show();
}
*/
