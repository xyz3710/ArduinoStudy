
#include "U8glib.h"

U8GLIB_SSD1306_128X64 u8g(2, 3, 5, 4);

int nTrigPin  = 11;
int nEchoPin  = 12;

int nDistance = 0;

void setup()
{
	pinMode(nTrigPin, OUTPUT);
	pinMode(nEchoPin, INPUT );
  Serial.begin(9600);
}

extern void draw(void);

long microsecondsToCenti(long microseconds)
{
	return microseconds / 29 / 2;
}

void loop()
{
	u8g.firstPage();
	do
	{
		draw();

	} while (u8g.nextPage());

	digitalWrite(nTrigPin, LOW );
	delayMicroseconds(2);

	digitalWrite(nTrigPin, HIGH);
	delayMicroseconds(10);

	digitalWrite(nEchoPin, LOW );

	long duration = pulseIn(nEchoPin, HIGH);

	nDistance = microsecondsToCenti(duration);
  Serial.println(nDistance);
	delay(100);
}

void draw(void)
{
	u8g.setFont(u8g_font_unifont);

	char sTemp[100] = "";
	sprintf(sTemp, "%d cm", nDistance);

	u8g.drawStr(0, 10, sTemp);
}
