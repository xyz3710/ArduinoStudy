int nAPin   = 11; // DO
int nBPin   = 12; // CLK
int nBtnPin = 10; // SW

void setup()
{
	pinMode(nAPin,   INPUT);
	pinMode(nBPin,   INPUT);
	pinMode(nBtnPin, INPUT);

	Serial.begin(9600);
}

void loop()
{
	
	int nChangeTurn = getEncoderTurn();
	if (nChangeTurn != 0)
	{
		Serial.print("Rotary : ");
		Serial.println(nChangeTurn);
	}
	

	int nChangeButton = getButtonPush();
	if (nChangeButton != 0)
	{
		Serial.print("Button : ");
		Serial.println(nChangeButton);
	}
	
	delay(1);
}

int getEncoderTurn()
{
	static int oldA = LOW;
	static int oldB = LOW;

	int nRes = 0;

	int newA = digitalRead(nAPin);
	int newB = digitalRead(nBPin);

	if (newA != oldA || newB != oldB)
	{
		if (oldA == LOW && newA == HIGH)
		{
			nRes = -(oldB * 2 - 1);
		}
	}

	oldA = newA;
	oldB = newB;

	return nRes;
	// -1, 0, 1    0�ΰ�� ������ ���� ��� 
}

int getButtonPush()
{
	static int oldC = HIGH;
	int nRes = 0;
	int newC = digitalRead(nBtnPin);

	if (newC != oldC )
	{
		nRes = -1;
		if (newC == LOW) nRes = 1;
		oldC = newC;
	}

	return nRes;
	// -1, 0, 1    0�ΰ�� ������ ���� ��� 
}

